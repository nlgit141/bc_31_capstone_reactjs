import thunk from "redux-thunk";
import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { userReducer } from "./reducers/userReducer";
import { movieReducer } from "./reducers/movieReducer";

const rootReducer = combineReducers({
    userReducer,
    movieReducer
});



const composeEnhanser = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(rootReducer, composeEnhanser(applyMiddleware(thunk)));