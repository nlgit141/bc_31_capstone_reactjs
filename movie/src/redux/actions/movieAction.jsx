
import { movieService } from '../../Services/movieService';
import { SET_MOVIE_LIST } from '../contants/movieContant';

export default function getMovieListActionService() {
    return (dispatch) => {
        movieService.getMovieList().then((res) => {
            console.log(res);
            dispatch({
                type: SET_MOVIE_LIST,
                payload: res.data.content
            });
        })
            .catch((err) => {
                console.log(err);
            });
    };

}
