import { localStorageService } from "../../Services/localStorageService";
import { LOGIN } from "../contants/userContant";

// import { LOGIN } from "../contants/userContant";
const initialState = {
    userInfor: localStorageService.user.get()

};

export let userReducer = (state = initialState, { type, payload }) => {

    switch (type) {
        case LOGIN: {
            state.userInfor = payload;
            return { ...state };
        }
        default:
            return state;
    };
};