import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';

export default function News() {
    const [state, setstate] = useState(true);

    let changeActiveNav = () => {
        setstate(!state);
    };


    let classNames = 'bg-gray-800 text-[#36ffe8] text-2xl py-1 px-2 rounded-md';

    return (
        <div
            className=" bg-slate-900  text-white"
            id="tinTuc"
        >
            <div className='h-20 flex items-center'>
                <div className=" max-w-7xl mx-auto    space-x-10 ">
                    <NavLink to="/" className={state ? classNames : "text-white text-2xl py-1 px-2 rounded-md"} >
                        <button onClick={() => {
                            changeActiveNav();
                        }} >Điện ảnh 24h</button>
                    </NavLink>
                    <NavLink to="/" className={!state ? classNames : "text-white text-2xl py-1 px-2 rounded-md"} >
                        <button onClick={() => {
                            changeActiveNav();
                        }} >Review</button>
                    </NavLink>

                </div>
            </div>
            <div
                role="tabpanel"
                id="simple-tabpanel-0"
                aria-labelledby="simple-tab-0"
            >
                <div className='max-w-7xl mx-auto my-5 bg-black p-2 '>
                    <div className="grid grid-cols-3 gap-10 wrapper spacing-xs-2">
                        <div className=" grid-cols-12 md:grid-cols-6">
                            <div className="jss114">
                                <a
                                    target="_blank"
                                    href="https://tix.vn/goc-dien-anh/7943-tenet-cong-bo-ngay-khoi-chieu-chinh-thuc-tai-viet-nam"
                                    rel="noreferrer"
                                >
                                    <img
                                        className="jss115"
                                        alt="poster"
                                        src="https://s3img.vcdn.vn/123phim/2020/07/tenet-cong-bo-ngay-khoi-chieu-chinh-thuc-tai-viet-nam-15959320391357.png"
                                    />
                                </a>
                            </div>
                            <a
                                className="jss116"
                                target="_blank"
                                href="https://tix.vn/goc-dien-anh/7943-tenet-cong-bo-ngay-khoi-chieu-chinh-thuc-tai-viet-nam"
                                rel="noreferrer"
                            >
                                <p className="md:text-xl text-red-500 hover:text-red-700 duration-150 py-1">
                                    TENET công bố ngày khởi chiếu
                                    chính thức tại Việt Nam
                                </p>
                            </a>
                            <p className="hidden md:block">
                                Đêm qua theo giờ Việt Nam, hãng
                                phim Warner Bros. đưa ra thông báo
                                chính thức về ngày khởi chiếu cho
                                bom tấn TENET tại các thị trường
                                bên ngoài Bắc Mỹ, trong đó có Việt
                                Nam.
                            </p>
                        </div>
                        <div className="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-6">
                            <div className="jss114">
                                <a
                                    target="_blank"
                                    href="https://tix.vn/goc-dien-anh/7941-khi-phu-nu-khong-con-o-the-tron-chay-cua-nan-nhan"
                                    rel="noreferrer"
                                >
                                    <img
                                        className="jss115"
                                        alt="poster"
                                        src="https://s3img.vcdn.vn/123phim/2020/07/khi-phu-nu-khong-con-o-the-tron-chay-cua-nan-nhan-15943683481617.jpg"
                                    />
                                </a>
                            </div>
                            <a
                                className="jss116"
                                target="_blank"
                                href="https://tix.vn/goc-dien-anh/7941-khi-phu-nu-khong-con-o-the-tron-chay-cua-nan-nhan"
                                rel="noreferrer"
                            >
                                <p className="md:text-xl text-red-500 hover:text-red-700 duration-150 py-1">
                                    Khi phụ nữ không còn ở thế
                                    trốn chạy của nạn nhân
                                </p>
                            </a>
                            <p className="hidden md:block">
                                Là bộ phim tâm lý li kỳ với chủ đề
                                tội phạm, Bằng Chứng Vô Hình mang
                                đến một góc nhìn mới về hình ảnh
                                những người phụ nữ thời hiện đại.
                                Điều đó được thể hiện qua câu
                                chuyện về hai người phụ nữ cùng hợp
                                sức để vạch mặt tên tội phạm có sở
                                thích bệnh hoạn với phụ nữ.
                            </p>
                        </div>
                        <div className="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-4">
                            <div className="jss114">
                                <a
                                    target="_blank"
                                    href="https://tix.vn/goc-dien-anh/7940-gerard-butler-cung-bo-cu-deadpool-tham-gia-greenland"
                                    rel="noreferrer"
                                >
                                    <img
                                        className="jss115"
                                        alt="poster"
                                        src="https://s3img.vcdn.vn/123phim/2020/07/gerard-butler-cung-bo-cu-deadpool-tham-gia-greenland-15937528932506.png"
                                    />
                                </a>
                            </div>
                            <a
                                className="jss116"
                                target="_blank"
                                href="https://tix.vn/goc-dien-anh/7940-gerard-butler-cung-bo-cu-deadpool-tham-gia-greenland"
                                rel="noreferrer"
                            >
                                <p className="md:text-xl text-red-500 hover:text-red-700 duration-150 py-4 font-bold ">
                                    Gerard Butler cùng bồ cũ
                                    Deadpool tham gia Greenland
                                </p>
                            </a>
                            <p className="hidden md:block truncate">
                                Bộ phim hành động mang đề tài tận
                                thế Greenland: Thảm Họa Thiên Thạch
                                đến từ nhà sản xuất của loạt phim
                                John Wick đã tung ra trailer đầu
                                tiên, hé lộ nội dung cốt truyện,
                                dàn diễn viên, cùng hàng loạt đại
                                cảnh cháy nổ hoành tráng.
                            </p><button className='text-blue-600 active:text-blue-900'><span>Đọc thêm</span></button>
                        </div>
                        <div className="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-md-4">
                            <div className="jss114">
                                <a
                                    target="_blank"
                                    href="https://tix.vn/goc-dien-anh/7939-dien-vien-dac-biet-cua-bang-chung-vo-hinh"
                                    rel="noreferrer"
                                >
                                    <img
                                        className="jss115"
                                        alt="poster"
                                        src="https://s3img.vcdn.vn/123phim/2020/07/dien-vien-dac-biet-cua-bang-chung-vo-hinh-15937518743844.png"
                                    />
                                </a>
                            </div>
                            <a
                                className="jss116"
                                target="_blank"
                                href="https://tix.vn/goc-dien-anh/7939-dien-vien-dac-biet-cua-bang-chung-vo-hinh"
                                rel="noreferrer"
                            >
                                <p className="md:text-xl text-red-500 hover:text-red-700 duration-150 py-1">
                                    Diễn viên đặc biệt của Bằng
                                    Chứng Vô Hình
                                </p>
                            </a>
                            <p className="hidden md:block">
                                Bằng Chứng Vô Hình tiết lộ thêm với
                                khán giả một diễn viên vô cùng đặc
                                biệt, đi diễn như đi chơi và không
                                hề nghe theo sự chỉ đạo của đạo
                                diễn Trịnh Đình Lê Minh. Đó chính
                                là chú chó Ben – trợ thủ đắc lực
                                của cô gái mù Thu (Phương Anh Đào).
                            </p>
                        </div>
                        <div className="overflow-x-scroll">
                            <div className="flex">
                                <div className="">
                                    <a
                                        target="_blank"
                                        href="https://tix.vn/goc-dien-anh/7938-pee-nak-2-van-kiep-thien-thu-di-tu-khong-het-nghiep"
                                        rel="noreferrer"
                                    >
                                        <img
                                            className="object-cover object-center"
                                            alt="poster"
                                            src="https://s3img.vcdn.vn/123phim/2020/07/pee-nak-2-van-kiep-thien-thu-di-tu-khong-het-nghiep-15937498464029.png"
                                        />
                                    </a>
                                </div>
                                <a
                                    className="jss120"
                                    target="_blank"
                                    href="https://tix.vn/goc-dien-anh/7938-pee-nak-2-van-kiep-thien-thu-di-tu-khong-het-nghiep"
                                    rel="noreferrer"
                                >
                                    <p className="text-md text-blue-300 hover:text-red-700 duration-150 py-1 truncate">
                                        Pee Nak 2 - Vạn kiếp
                                        thiên thu, đi tu không
                                        hết nghiệp!
                                    </p>
                                </a>
                            </div>
                            <div className="flex">
                                <div className="jss119">
                                    <a className="object-cover object-center"
                                        target="_blank"
                                        href="https://tix.vn/goc-dien-anh/7937-loat-phim-kinh-di-khong-the-bo-lo-trong-thang-7"
                                        rel="noreferrer"
                                    >
                                        <img
                                            className="jss115"
                                            alt="poster"
                                            src="https://s3img.vcdn.vn/123phim/2020/07/loat-phim-kinh-di-khong-the-bo-lo-trong-thang-7-15937470779379.png"
                                        />
                                    </a>
                                </div>
                                <a className="object-cover object-center"
                                    target="_blank"
                                    href="https://tix.vn/goc-dien-anh/7937-loat-phim-kinh-di-khong-the-bo-lo-trong-thang-7"
                                    rel="noreferrer"
                                >
                                    <p className="MuiTypography-root jss121 MuiTypography-body1">
                                        Loạt phim kinh dị không
                                        thể bỏ lỡ trong tháng 7!
                                    </p>
                                </a>
                            </div>
                            <div className="flex">
                                <div className="jss119">
                                    <a className="object-cover object-center"
                                        target="_blank"
                                        href="https://tix.vn/goc-dien-anh/7936-rom-tung-trailer-he-lo-cuoc-song-cua-dan-choi-so-de"
                                        rel="noreferrer"
                                    >
                                        <img
                                            className="jss115"
                                            alt="poster"
                                            src="https://s3img.vcdn.vn/123phim/2020/06/rom-tung-trailer-he-lo-cuoc-song-cua-dan-choi-so-de-15929959532579.jpg"
                                        />
                                    </a>
                                </div>
                                <a
                                    className="jss120"
                                    target="_blank"
                                    href="https://tix.vn/goc-dien-anh/7936-rom-tung-trailer-he-lo-cuoc-song-cua-dan-choi-so-de"
                                    rel="noreferrer"
                                >
                                    <p className="MuiTypography-root jss121 MuiTypography-body1">
                                        RÒM tung trailer hé lộ
                                        cuộc sống của dân chơi số
                                        đề
                                    </p>
                                </a>
                            </div>
                            <div className="flex">
                                <div className="jss119">
                                    <a
                                        target="_blank"
                                        href="https://tix.vn/goc-dien-anh/7935-antebellum-trailer-cuoi-cung-khong-he-lo-bat-cu-thong-tin-gi-them"
                                        rel="noreferrer"
                                    >
                                        <img
                                            className="jss115"
                                            alt="poster"
                                            src="https://s3img.vcdn.vn/123phim/2020/06/antebellum-trailer-cuoi-cung-khong-he-lo-bat-cu-thong-tin-gi-them-15929866818923.jpg"
                                        />
                                    </a>
                                </div>
                                <a
                                    className="jss120"
                                    target="_blank"
                                    href="https://tix.vn/goc-dien-anh/7935-antebellum-trailer-cuoi-cung-khong-he-lo-bat-cu-thong-tin-gi-them"
                                    rel="noreferrer"
                                >
                                    <p className="MuiTypography-root jss121 MuiTypography-body1">
                                        Antebellum - Trailer cuối
                                        cùng không hé lộ bất cứ
                                        thông tin gì thêm
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div
                role="tabpanel"
                id="simple-tabpanel-1"
                aria-labelledby="simple-tab-1"
                hidden
            />
            <div
                role="tabpanel"
                id="simple-tabpanel-2"
                aria-labelledby="simple-tab-2"
                hidden
            />
            <div className="jss122">
                <button
                    className="MuiButtonBase-root MuiButton-root MuiButton-outlined jss123"
                    tabIndex={0}
                    type="button"
                >
                    <span className="MuiButton-label">RÚT GỌN</span>
                    <span className="MuiTouchRipple-root" />
                </button>
            </div>
        </div >
    );
}
