import { Tabs } from 'antd';
import TabPane from 'antd/lib/tabs/TabPane';
import React, { useState, useEffect } from 'react';
import { movieService } from '../../Services/movieService';
import ItemTabMovie from './ItemTabMovie';
import "./TabMovie.css";
export default function TabMovie() {
    const [dataMovieTheater, setDataMovieTheater] = useState([]);


    useEffect(() => {
        movieService
            .getMovieByTheater()
            .then((res) => {
                console.log(res);
                setDataMovieTheater(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    let renderMovieByTheater = () => {
        return dataMovieTheater.map((heThongRap, index) => {
            return (
                <TabPane className='shadow-sm-light bg-[#202020]' tab={<div className='border-2 rounded-full '><img src={heThongRap.logo} alt="" width="45px" /></div>} key={index}>
                    <Tabs style={{ height: 500, color: "white" }} tabPosition="left" defaultActiveKey="1" className=' bg-[#202020]  shadow-sm-light' >
                        {heThongRap.lstCumRap.map((cumRap, index) => {
                            return <TabPane tab={<div className='hover:bg-slate-800'>{renderTenCumRap(cumRap)
                            }</div>} key={cumRap.maCumRap}>
                                <div style={{ height: 500, overflowY: "scroll" }} className="shadow-sm-light">
                                    {cumRap.danhSachPhim.map((phim, index) => {
                                        return <ItemTabMovie phim={phim} key={index} />;
                                    })}
                                </div>
                            </TabPane>;
                        })}
                    </Tabs>
                </TabPane>
            );

        });
    };



    const renderTenCumRap = (cumRap) => {
        return <div className='text-left w-60 whitespace-normal'>
            <p className="text-[#24a3ac] truncate active:text-[#00ffea]  ">{cumRap.tenCumRap}</p>
            <p className="text-white truncate">{cumRap.diaChi}</p>
            <button className='text-red-500'>[Xem chi tiết] {`{ }`}</button>
        </div>;
    };


    return (
        <div className='max-w-7xl mx-auto my-12'  >
            <div className="bg-[#202020]">
                <div div className="h-12 sm:h-20 px-2 sm:px-4 lg:px-4  max-w-7xl mx-auto flex justify-between items-center" >
                    <p className='text-white md:text-xl font-extrabold uppercase' >Cụm rạp</p>
                    <p className='text-red-700 md:text-xl font-extrabold uppercase' >Lịch chiếu</p>

                </div >
            </div >
            <Tabs tabPosition="left" defaultActiveKey="1" className='scroll bg-[#202020] shadow-sm-light rounded-lg py-5 mt-5 '  >
                {renderMovieByTheater()}
            </Tabs>
        </div>
    );
}
