import moment from 'moment';
import React from 'react';


export default function ItemTabMovie({ phim }) {
    return (

        <div className='flex space-x-2 bg-neutral-800'>
            <div className='w-32 h-44 m-1 p-2 bg-[#202020] rounded-md '>
                <img className=' h-full w-full object-cover ' src={phim.hinhAnh} alt="" />
            </div>

            <div className='bg-[#202020] my-1 p-2 rounded-md'>
                <p className='font-medium text-2xl text-white mb-5'>{phim.tenPhim}</p>
                <div className='grid grid-cols-4 gap-4'>
                    {phim.lstLichChieuTheoPhim.slice(0, 8).map((lichChieu, index) => {
                        return <div key={index} className='bg-red-800 text-white p-1 rounded'>{moment(lichChieu.ngayChieuGioChieu).format("DD-MM-YYYY")}
                            <span className='text-yellow-300 font-bold text-base ml-7'>
                                {moment(lichChieu.ngayChieuGioChieu).format("hh:mm")}
                            </span>
                        </div>;
                    })}
                </div>
            </div>
        </div>


    );
}