import React, { Component } from "react";
import CarouselMovie from "../../Components/Carousel/CarouselMovie";
import Footer from "../../Components/Footer/Footer";
import Sidebar from "../../Components/Sidebar/Sidebar";
import ListMovie from "./ListMovie";
import News from "./News";
import TabMovie from "./TabMovie";


export default class HomePage extends Component {
    render() {
        return (
            <div className="">
                <div div className=" rounded-lg md:p-5 " >
                    <CarouselMovie />
                </div >
                <div className="">
                    <ListMovie />
                </div>
                <TabMovie />
                <div className="bg-white">
                    <News />
                </div>
                <Footer />
            </div >);
    }
};