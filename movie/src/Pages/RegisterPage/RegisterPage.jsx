import React from 'react';
import logoCineMax from "../../assets/Logo-cinemax.png";
import { LockClosedIcon } from '@heroicons/react/24/outline';
import { Checkbox, Form, Input, message, Select, } from 'antd';
import { useNavigate } from 'react-router-dom';
import { userService } from '../../Services/userService';



const { Option } = Select;

export default function RegisterPage() {
    let navigate = useNavigate();

    const [form] = Form.useForm();

    const onFinish = (values) => {
        console.log('Received values of form: ', values);
        userService.postRegister(values)
            .then((res) => {
                console.log(res);
                if (res.data) {
                    message.success("Đăng ký thành công");
                    setTimeout(() => {
                        navigate("/login");
                    }, 3000);
                    return (res.data);
                }

            })
            .catch((err) => {
                console.log(err);
                return (err.response.data.content);
            });


    };



    const prefixSelector = (
        <Form.Item name="prefix" noStyle>
            <Select
                style={{
                    width: 70,
                }}
            >
                <Option value="84">+84</Option>
            </Select>
        </Form.Item>
    );


    return (
        <div className='bg-[#000000] py-10'>
            <div className="mx-auto max-w-md space-y-8 bg-slate-900  p-5 rounded-lg "  >
                <div className='flex flex-col  items-center '>
                    <img src={logoCineMax} alt="" />
                    <p className='text-white text-center text-4xl font-bold'>Đăng ký</p>
                </div>
                <div className=''>
                    <Form
                        layout='vertical'
                        // {...formItemLayout}
                        form={form}
                        name="register"
                        onFinish={onFinish}
                        initialValues={{
                            prefix: '84',
                        }}
                        scrollToFirstError
                    >

                        {/* Tài khoản */}
                        <Form.Item
                            type="string"
                            name="taiKhoan"
                            tooltip="What do you want others to call you?"
                            rules={[
                                {
                                    required: true,
                                    message: 'vui lòng nhập tài khoản!',
                                    whitespace: true,
                                },
                            ]}
                        >
                            <Input placeholder='Tài khoản' className='rounded-md' />
                        </Form.Item>

                        {/* Mật khẩu */}
                        <Form.Item
                            type="string"
                            name="matKhau"
                            rules={[
                                {
                                    required: true,
                                    message: 'vui lòng nhập mật khẩu!',
                                },
                            ]}
                            hasFeedback
                        >
                            <Input.Password placeholder='Mật khẩu' className='rounded-md h-10' />
                        </Form.Item>

                        {/* Email */}
                        <Form.Item
                            type="string"
                            name="email"
                            rules={[
                                {
                                    type: 'email',
                                    message: 'The input is not valid E-mail!',
                                },
                                {
                                    required: true,
                                    message: 'vui lòng nhập E-mail!',
                                },
                            ]}
                        >
                            <Input
                                placeholder='Email' className='rounded-md'
                            />
                        </Form.Item>

                        {/* Số-ĐT */}
                        <Form.Item
                            type="string"
                            name="soDT"
                            rules={[
                                {
                                    required: true,
                                    message: 'vui lòng nhập số điện thoại!',
                                },
                            ]}
                        >
                            <Input placeholder='Số ĐT'
                                addonBefore={prefixSelector}
                                style={{
                                    width: '100%',
                                }}
                                className='rounded-md'
                            />
                        </Form.Item>

                        {/* Mã nhóm */}
                        {/* <Form.Item
                            type="string"
                            name="maNhom"
                            tooltip="What do you want others to call you?"
                            rules={[
                                {
                                    required: true,
                                    message: 'vui lòng nhập mã nhóm!',
                                    whitespace: true,
                                },
                            ]}
                        >
                            <Input placeholder='Mã nhóm' className='rounded-md' />
                        </Form.Item> */}


                        {/* Họ-Tên */}
                        <Form.Item
                            type="string"
                            name="hoTen"
                            tooltip="What do you want others to call you?"
                            rules={[
                                {
                                    required: true,
                                    message: 'vui lòng nhập họ tên!',
                                    whitespace: true,
                                },
                            ]}
                        >
                            <Input placeholder='Họ tên' className='rounded-md' />
                        </Form.Item>


                        <Form.Item
                            name="agreement"
                            valuePropName="checked"
                            rules={[
                                {
                                    validator: (_, value) =>
                                        value ? Promise.resolve() : Promise.reject(new Error('Should accept agreement')),
                                },
                            ]}>
                            <Checkbox className='text-white'>
                                I have read the <a href="">agreement</a>
                            </Checkbox>
                        </Form.Item>

                        <button
                            type="submit"
                            className="group relative flex w-full justify-center rounded-md border border-transparent bg-[#e5b60d] py-2 px-4 text-sm font-medium text-black hover:text-[#e5b60d] hover:bg-gray-700 hover:duration-500  focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:ring-offset-2"
                        >
                            <span className="absolute inset-y-0 left-0 flex items-center pl-3">
                                <LockClosedIcon className="h-5 w-5 text-white group-hover:text-[#FF5505]" aria-hidden="true" />
                            </span>
                            Tạo tài khoản
                        </button>
                    </Form>
                </div>
            </div>
        </div >

    );
};