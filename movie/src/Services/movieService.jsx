import { https } from "./configURL";


export let movieService = {
    getBannerMovie: () => {
        return https.get("api/QuanLyPhim/LayDanhSachBanner");
    },
    getMovieList: () => {
        return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP10");
    },
    getMovieDetail: (id) => {
        return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`);
    },
    getMovieByTheater: () => {
        return https.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP10");
    }

};