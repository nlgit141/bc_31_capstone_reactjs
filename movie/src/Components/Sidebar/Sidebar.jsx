import {
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    UploadOutlined,
    UserOutlined,
    VideoCameraOutlined,
} from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import React, { useState } from 'react';
import CarouselMovie from '../Carousel/CarouselMovie';
const { Header, Sider, Content } = Layout;

const SideBar = () => {
    const [collapsed, setCollapsed] = useState(false);
    return (
        <Layout>
            <Sider Sider className=' flex h-screen mt-20 hidden md:inline-block' trigger={null} collapsible collapsed={collapsed} >
                <Menu
                    className='sm:block '
                    theme="dark"
                    mode="inline"
                    defaultSelectedKeys={['1']}
                    items={[
                        {
                            key: '1',
                            icon: <UserOutlined />,
                            label: 'Trang chủ',
                        },
                        {
                            key: '2',
                            icon: <VideoCameraOutlined />,
                            label: 'nav 2',
                        },
                        {
                            key: '3',
                            icon: <UploadOutlined />,
                            label: 'nav 3',
                        },
                    ]}
                />
            </Sider >
            <Layout className="site-layout">
                <Header
                    className="site-layout-background"
                    style={{
                        padding: 0,
                    }}
                >
                    {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                        className: 'trigger',
                        onClick: () => setCollapsed(!collapsed),
                    })}
                </Header>
                <Content
                    className="site-layout-background"

                >
                    <CarouselMovie />
                </Content>
            </Layout>
        </Layout >
    );
};

export default SideBar;
// #components - layout - demo - custom - trigger.trigger {
//     padding: 0 24px;
//     font - size: 18px;
//     line - height: 64px;
//     cursor: pointer;
//     transition: color 0.3s;
// }

// #components - layout - demo - custom - trigger.trigger:hover {
//     color: #1890ff;
// }

// #components - layout - demo - custom - trigger.logo {
//     height: 32px;
//     margin: 16px;
//     background: rgba(255, 255, 255, 0.3);
// }

// .site - layout.site - layout - background {
//     background: #fff;
// }