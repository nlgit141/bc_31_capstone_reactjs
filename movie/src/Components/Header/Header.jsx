import React from 'react';
import logoCineMAx from "../../assets/Logo-cinemax.png";
import UserNav from '../UserNav/UserNav';
import { Disclosure } from '@headlessui/react';
import { Bars3Icon, XMarkIcon } from '@heroicons/react/24/outline';
import { NavLink } from 'react-router-dom';

const navigation = [
    { name: 'Trang chủ', href: '/', current: true },
    { name: 'Lịch chiếu', href: '#', current: false },
    { name: 'Cụm rạp', href: '#', current: false },
    { name: 'Tin tức', href: '#', current: false },
    { name: 'Ứng dụng', href: '#', current: false },
];

function classNames(...classes) {
    return classes.filter(Boolean).join(' ');
}

export default function Header() {
    return (
        <div>
            <Disclosure Disclosure as="nav" className="bg-[#202020] fixed bg-fixed z-10 max-w-7xl min-w-full shadow-rose-600 shadow-md  " >
                {({ open }) => (
                    <>
                        <div className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
                            <div className="relative flex h-[90px] items-center justify-between">
                                <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                                    {/* Mobile menu button*/}
                                    <Disclosure.Button className="inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-700 hover:text-white focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                                        <span className="sr-only">Open main menu</span>
                                        {open ? (
                                            <XMarkIcon className="block h-6 w-6" aria-hidden="true" />
                                        ) : (
                                            <Bars3Icon className="block h-6 w-6" aria-hidden="true" />
                                        )}
                                    </Disclosure.Button>
                                </div>
                                <div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
                                    <div className="flex flex-shrink-0 items-center">
                                        <img
                                            className="block h-12 w-auto lg:hidden"
                                            src={logoCineMAx}
                                            alt="logoCineMAx"
                                        />
                                        <img
                                            className="hidden h-12 w-auto lg:block"
                                            src={logoCineMAx}
                                            alt="logoCineMAx"
                                        />
                                    </div>
                                    <div className="hidden pt-[4px] sm:ml-6 sm:block ">
                                        <div className="flex space-x-4">
                                            <input type="text" className='rounded-full bg-[#2b2c33] h-8 hidden w-60 lg:block px-5 font-medium focus:outline-none focus:shadow-outline focus:border-red-300' placeholder='Tìm kiếm' />
                                            {navigation.map((item) => (
                                                <NavLink to={item.href} key={item.name} className={classNames(
                                                    item.current ? 'bg-gray-800 text-[#36ffe8]' : 'text-gray-300 hover:bg-gray-800 hover:text-[#33ff18]',
                                                    'px-3 py-2 rounded-md text-sm font-medium'

                                                )}
                                                    aria-current={item.current ? 'page' : undefined}>
                                                    {item.name}
                                                </NavLink>

                                            ))}
                                        </div>
                                    </div>
                                </div>
                                <UserNav />

                            </div>
                        </div>
                        <Disclosure.Panel className="sm:hidden">
                            <div className="space-y-1 px-2 pt-2 pb-3">
                                {navigation.map((item) => (
                                    <Disclosure.Button
                                        key={item.name}
                                        as="a"
                                        href={item.href}
                                        className={classNames(
                                            item.current ? 'bg-gray-900 text-white' : 'text-gray-300 hover:bg-gray-700 hover:text-white',
                                            'block px-3 py-2 rounded-md text-base font-medium'
                                        )}
                                        aria-current={item.current ? 'page' : undefined}
                                    >
                                        {item.name}
                                    </Disclosure.Button>
                                ))}
                            </div>
                        </Disclosure.Panel>
                    </>
                )
                }
            </Disclosure >
        </div >
    );
}