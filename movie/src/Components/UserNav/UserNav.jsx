import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { UserOutlined } from '@ant-design/icons';
import { Avatar } from 'antd';
import { Fragment } from 'react';
import { Menu, Transition } from '@headlessui/react';
import { ArrowRightOnRectangleIcon, BellIcon } from '@heroicons/react/24/outline';
import { NavLink } from 'react-router-dom';
import { localStorageService } from '../../Services/localStorageService';
import { loginAction } from '../../redux/actions/userAction';
import { MobileReponsive } from '../../HOC/ReponsiveComponent';

function classNames(...classes) {
    return classes.filter(Boolean).join(' ');
}


export default function UserNav() {

    let dispatch = useDispatch();
    let userInfor = useSelector((state) => {
        return state.userReducer.userInfor;
    });

    let renderContent = () => {
        if (userInfor) {
            return (<div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                <button
                    type="button"
                    className="rounded-full bg-gray-800 p-1 text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-800"
                >
                    <span className="sr-only">View notifications</span>
                    <BellIcon className="h-6 w-6" aria-hidden="true" />
                </button>

                {/* Profile dropdown */}
                <Menu as="div" className="relative ml-3">
                    <div className='flex space-x-2'>
                        <Menu.Button className="flex rounded-full bg-gray-800 text-sm focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-800">
                            <span className="sr-only">Open user menu</span>
                            {< Avatar className="h-8 w-8 rounded-full bg-[#FF5605]" icon={<UserOutlined />} /> || <img
                                className="h-8 w-8 rounded-full"
                                src={userInfor.hinhAnh}
                                alt="avatar"
                            />}

                            <div className='hidden lg:block text-[#87FDFF] uppercase font-bold px-4 py-1'>{userInfor.hoTen}</div>
                        </Menu.Button>
                        <NavLink className="text-white font-bold"
                            onClick={handleLogout}
                        ><ArrowRightOnRectangleIcon className="h-8 w-8 hidden lg:block" />
                        </NavLink>
                    </div>

                    <Transition
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                    >
                        <Menu.Items className="absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                            <Menu.Item className="lg:hidden">
                                {({ active }) => (
                                    <NavLink to={"/"}

                                        className={classNames(active ? 'bg-gray-100' : '', 'block px-4 py-2 text-sm text-gray-700')}
                                    >
                                        <p>{userInfor.hoTen}</p>
                                    </NavLink>
                                )}
                            </Menu.Item>


                            <MobileReponsive>
                                <Menu.Item  >
                                    {({ active }) => (
                                        <NavLink
                                            onClick={handleLogout}
                                            className={classNames(active ? 'bg-gray-100' : '', 'block px-4 py-2 text-sm text-gray-700 flex justify-between')}
                                        >
                                            Đăng xuất <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                                <path strokeLinecap="round" strokeLinejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9" />
                                            </svg>

                                        </NavLink>
                                    )}
                                </Menu.Item>
                            </MobileReponsive>

                        </Menu.Items>
                    </Transition>
                </Menu>
            </div >);
        } else {

            return (
                <div className='flex space-x-3'>
                    <NavLink to="/login">
                        <button className=' text-white bg-[#FF0505]  py-2 px-4 rounded-lg font-bold hover:bg-gray-700 hover:text-white duration-300'>Đăng nhập</button>
                    </NavLink>

                    <NavLink to="/register">
                        <button className='text-black  bg-[#e5b60d] py-2 px-4 rounded-lg font-bold'>Đăng ký</button>
                    </NavLink>
                </div >);
        }

    };

    let handleLogout = () => {
        localStorageService.user.remove();
        dispatch(loginAction(null));
    };


    return (
        <div>
            {renderContent()}
        </div>
    );
}
