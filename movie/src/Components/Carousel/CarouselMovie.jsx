import { Carousel } from 'flowbite-react';
import React, { useEffect } from 'react';
import { movieService } from '../../Services/movieService';
import { useState } from "react";




export default function CarouselMovie() {
    const [movieBanner, setMovieBanner] = useState([]);
    useEffect(() => {
        movieService
            .getBannerMovie()
            .then((res) => {
                console.log(res.data.content);
                return setMovieBanner(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    let renderContentBannerMovie = () => {
        return movieBanner.map((item) => {
            return (
                <img key={item.maPhim} className='w-5/6 h-4/5 object-top'
                    src={item.hinhAnh}
                    alt={item.hinhAnh}
                />

            );
        });
    };



    return (
        <div className=" h-72 md:h-[40vh]  xl:h-[80vh] 2xl:h-screen container mx-auto  ">
            <Carousel slideInterval={5000} >
                {renderContentBannerMovie()}
            </Carousel>
        </div>

    );
};




